package com.example.fileservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.core.io.Resource;

@Data
@AllArgsConstructor
public class FileInfo {
    private String fileName;
    private Resource resource;
}
