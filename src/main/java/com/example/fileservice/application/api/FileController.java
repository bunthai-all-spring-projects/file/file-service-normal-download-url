package com.example.fileservice.application.api;

import com.example.fileservice.domain.FileInfo;
import com.example.fileservice.service.FileServiceImp;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

@RestController
@RequestMapping("v1/file")
class FileController {

    Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    FileServiceImp fileService;

    @PostMapping("/upload")
    public Map upload(@RequestParam("file") MultipartFile file) throws IOException {
        Map fileInfo = fileService.upload(file);
        Path path = fileService.loadPath(fileInfo.get("fileKey").toString());
        String url = MvcUriComponentsBuilder.fromMethodName(FileController.class, "downloadAsByte", fileInfo.get("fileKey").toString()).build().toString();
        fileInfo.put("url", url);
        return fileInfo;

    }

    @GetMapping("/{fileKey}/download/bytes")
    public ResponseEntity<Resource>  downloadAsByte(@PathVariable("fileKey") String fileKey) throws IOException {
        FileInfo fileInfo = fileService.load(fileKey);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileInfo.getFileName() + "\"").body(fileInfo.getResource());

    }

}