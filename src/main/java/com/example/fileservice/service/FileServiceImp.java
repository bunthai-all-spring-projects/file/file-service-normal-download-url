package com.example.fileservice.service;

import com.example.fileservice.domain.FileInfo;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class FileServiceImp {

    final Path uploadPath = Paths.get("upload");

    public Map upload(MultipartFile multipartFile) throws IOException {

        FileWriter myWriter = new FileWriter("data.txt", true);
        String uuid = UUID.randomUUID().toString();
        Map fileInfo = new HashMap();
        fileInfo.put("fileKey", uuid);
        fileInfo.put("fileName", multipartFile.getOriginalFilename());
        fileInfo.put("fileSize", multipartFile.getSize());
        myWriter.append( uuid + ";" + multipartFile.getOriginalFilename() + ";" + multipartFile.getSize()+"\n");
        myWriter.close();

        File directory = new File(uploadPath.toString());
        if(!directory.exists())
            directory.mkdir();
        Files.copy(multipartFile.getInputStream(), uploadPath.resolve(uuid));
//        Path file = uploadPath.resolve(uuid);
//        this.uploadPath.relativize(file);
//        MvcUriComponentsBuilder.
//        FileUtils.cleanDirectory()
//        fileInfo.put("url", new UrlResource(file.toUri()).toString());
//        System.out.println();
        return fileInfo;
    }

    public Path loadPath(String fileKey) throws IOException {
        Path file = uploadPath.resolve(fileKey);
        return uploadPath.relativize(file);
    }

    public FileInfo load(String fileKey) {
        try {
            Path file = uploadPath.resolve(fileKey);
            Resource resource = new UrlResource(file.toUri());

            String[] fileName = readFileInfoByFileKey(fileKey);

            if ((resource.exists() || resource.isReadable()) && fileName != null) {
                return new FileInfo(fileName[1], resource);
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Could not read the file!");
    }

    public byte[] downloadAsByte(String fileKey) throws IOException {
//        System.out.println(Files.read);

        File file = getFileByFileKey(fileKey);
        System.out.println(Files.readAllBytes(file.toPath()));
        return Files.readAllBytes(file.toPath());
    }

    public File getFileByFileKey(String fileKey) throws IOException {
        File file = null;
        try {
            File dir = new File(uploadPath.toString());
            file = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.startsWith(fileKey);
                }
            })[0];
        } catch (ArrayIndexOutOfBoundsException e){
            throw new IOException("file not found");
        }
        return file;
    }

    public String[] readFileInfoByFileKey(String fileKey) throws IOException {
        FileReader myReader = new FileReader("data.txt");
        BufferedReader br = new BufferedReader(myReader);
        String line;
        while ((line = br.readLine()) != null) {
            if(line.contains(fileKey))
                return line.split(";");
        }
        return null;
    }


}
